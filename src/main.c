#define _DEFAULT_SOURCE

#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

int main() {
    void* heap = heap_init(REGION_MIN_SIZE);

    uint8_t* block = _malloc(64);
    _free(block);
    printf("Simple malloc test PASSED\n");

    uint8_t* blocks[5] = {NULL};
    for (size_t i = 0; i < 5; i++) {
        blocks[i] = _malloc(64);
    }
    _free(blocks[1]);
    printf("Single free test PASSED\n");

    _free(blocks[3]);
    _free(blocks[4]);
    printf("Multiple free test PASSED\n");
    _free(blocks[0]);
    _free(blocks[2]);

    block = _malloc(4 * REGION_MIN_SIZE);
    printf("Region extend test PASSED\n");
    _free(block);
    munmap(heap, REGION_MIN_SIZE * 16);

    heap = heap_init(REGION_MIN_SIZE);
    void* taken_area = mmap(CONFLICT_ADDR, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                            MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    block = _malloc(4 * REGION_MIN_SIZE);
    printf("Region non-extending alloc test PASSED\n");
    _free(block);

    munmap(heap, REGION_MIN_SIZE * 16);
    munmap(taken_area, REGION_MIN_SIZE);

    printf("--------------------------------------\nAll tests PASSED!\n");
    return 0;
}
