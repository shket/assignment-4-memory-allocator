CC=gcc
CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG

PROGNAME=main
BUILDDIR=build
SRCDIR=src


$(BUILDDIR)/%.o: $(SRCDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@


.PHONY: all clean test

all: $(BUILDDIR)/mem.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/util.o $(BUILDDIR)/main.o
	$(CC) $^ -o $(BUILDDIR)/$(PROGNAME)

build:
	mkdir -p $(BUILDDIR)

clean:
	rm -rf $(BUILDDIR)

test:
	@+cd tester; make CC=$(CC)
